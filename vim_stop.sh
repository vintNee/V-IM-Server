#!/usr/bin/env bash
PID=$(ps -ef | grep V-IM-server-0.0.1-SNAPSHOT.jar | grep -v grep | awk '{ print $2 }')
if [ -z "$PID" ]
then
    echo 服务已关闭
else
    echo 关闭服务中 $PID
    kill $PID
fi